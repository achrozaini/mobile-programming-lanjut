import {StyleSheet, Text, TextInput, TouchableOpacity, View} from 'react-native';
import React, {Component} from 'react';

class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      pertama: '',
      kedua: '',
      fullText: '',
    };
  }

  gabungInput = () => {
    const pertama = this.state.pertama;
    const kedua = this.state.kedua;
    this.setState({fullText: pertama + " " + kedua});
  };

  render() {
    return (
      <View style={styles.container}>
        <TextInput style={styles.form} onChangeText={pertama => this.setState({pertama})} />
        <TextInput style={styles.form} onChangeText={kedua => this.setState({kedua})} />

        <Text>{this.state.fullText}</Text>

        <TouchableOpacity style={styles.btn1} onPress={() => this.gabungInput()}>
          <Text style={styles.btn2}>Nama Lengkap</Text>
        </TouchableOpacity>
      </View>
    );
  }
}

export default App;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  form: {
    backgroundColor: 'black',
    margin: 5,
    width: '100%',
    paddingHorizontal: 10,
    color: 'white',
  },
  btn1: {
    backgroundColor: 'green',
    padding: 12,
    margin: 10,
  },
  btn2: {
    color: 'white',
  },
});
